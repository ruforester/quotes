/// <reference path="../typings/main.d.ts" />

function setTweet(indx) {
    $("#btnTweet").attr("href", "https://twitter.com/intent/tweet?text=" + $("#quote" + indx).html() + " - " + $("#author" + indx).html());

    // console.log("#quote" + indx) ;
    // console.log($("#quote" + indx).html());
}


function getQuote(indx) {
    $.ajax({
        url: 'https://andruxnet-random-famous-quotes.p.mashape.com/?cat=famous',
        type: 'GET',
        data: {},
        dataType: 'json',
        success: function(data) {

            var hiddenQuoteIndex = (indx ? 0 : 1);

            $("#quote" + hiddenQuoteIndex).html(data['quote']);
            $("#author" + hiddenQuoteIndex).html(data['author']);

        },
        error: function(err) { alert(err); },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-Mashape-Authorization", "acIbT1pFdhmshxBsUj8QsWixJ8Vcp1RuEFcjsnLa1BN9tLcj0n");
        }
    });
}

$(document).ready(function() {
    $('#quote-carousel').carousel({
        pause: true,
        interval: false,
    });

    getQuote(0);
    setTweet(0);


    $('#quote-carousel').on('slide.bs.carousel', function(ev) {
        var quoteIndex = $(ev.relatedTarget).index();
        getQuote(quoteIndex);
        setTweet(quoteIndex);
    })
});